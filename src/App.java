import java.util.ArrayList;
import com.devcamp.javabasic.Person;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Task 52.10");
        ArrayList<Person> listPersons = new ArrayList<Person>();        

        Person person1 = new Person();
        // person1.name = "Nguyen Van Nam"; //Gán thủ công
        // person1.age = 10; // Cách gán thủ công
        // person1.showInfo();

        Person person2 = new Person("Nguyen Van B", 35, 40);
        // person2.showInfo();

        Person person3 = new Person("Nguyen Van C", 25, 40, 15000000);
        // person3.showInfo();
        
        Person person4 = new Person("Nguyen Van D", 28, 50, new String[] {"Cat", "Dog"});
        // person4.showInfo();

        Person person5 = new Person("Nguyen Van E", 36, 65, 20000000, new String[] {"Cat", "Dog", "Duck"});
        // person5.showInfo();
        
        //Add cac person vao arraylist
        listPersons.add(person1);
        listPersons.add(person2);
        listPersons.add(person3);
        listPersons.add(person4);
        listPersons.add(person5);
        // for (int i = 0; i<listPersons.size(); i++) {
            // System.out.println(listPersons.get(i));
        // }        
        for(Person person : listPersons) {
            //person.showInfo();
            System.out.println(person);
        }
    }    
}
