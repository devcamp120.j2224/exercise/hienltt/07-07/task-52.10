package com.devcamp.javabasic;

public class Person {
    public String name;//ten
    public int age;//tuoi
    public double weight;//can nang
    public long salary;//thu nhap
    public String[] pets;//danh sach con vat    

    /**
     * Ham khoi tao khong co tham so
     */
    public Person() {
        this.name = "Nguyen Van A";
        this.age = 20;
        this.weight = 30;
        this.salary = 10000000;
        this.pets = new String[] {"Dog", "Cat", "Chicken"};
    }

    /*
     * Ham khoi tao co 3 tham so
     */
    public Person(String name, int age, double weight) {
        this.name = name;
        this.age = age;
        this.weight = weight;
    }

    /*
     * Ham khoi tao co 4 tham so
     */
    public Person(String name, int age, double weight, long salary ) {
        // this.name = name;
        // this.age = age;
        // this.weight = weight;
        this(name, age, weight);
        this.salary = salary;
    }

    /*
     * Ham khoi tao co 4 tham so
     */
    public Person(String name, int age, double weight, String[] pets ) {
        this(name, age, weight);
        this.pets = pets;
    }

    /*
     * Ham khoi tao co 5 tham so
     */
    public Person(String name, int age, double weight, long salary, String[] pets ) {
        this(name, age, weight, salary);
        this.pets = pets;
    }
    
    /*
     * Phuong thuc hien thi du lieu
     * 
     */
    public void showInfo() {
        System.out.println("Name: " + this.name);
        System.out.println("Age: " + this.age);
        System.out.println("Weight: " + this.weight);
        System.out.println("Salary: " + this.salary);
        System.out.println("Pets: " + this.pets);
    }

    /*
     * Override: phuong thuc giong het lop cha (giong ca ten va tham so)
     */
    //@Override
    public String toString() {
        String strPerson = "{";
        strPerson += "Name: " + this.name + ",";
        strPerson += "Age: " + this.age + ",";
        strPerson += "Weight: " + this.weight + ",";
        strPerson += "Salary: " + this.salary + ",";
        strPerson += "Pets:[";
        if (this.pets != null) {
            for(String pet : pets) {
                strPerson += pet + ",";
            }
        }
        strPerson += "]}";
        return strPerson;
    }
}
